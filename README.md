Known as ‘The Mind Doctor’ having extensive education in the mechanics of the mind and all areas of human behavior, Dr. Ronda has assisted thousands of cases through grief, trauma, addiction recovery, family dynamics… to physical maladies and beyond.

Address: 601 University Ave, Sacramento, CA 95825, USA

Phone: 916-922-3100

Website: https://helpmedrronda.com
